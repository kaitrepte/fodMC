- 01_atoms
    * Initialization for atoms
- 02_molecules
    * Initialization for molecules, starting from xyz file
- 03_mol2fodmc
    * Initialization for atoms and molecules, starting from mol files (containing bonding information)
- 04_polyacetylene
    * Initialization for larger molecules, starting from mol files (containing bonding information)
