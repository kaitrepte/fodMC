# DATE USER CHANGE

#### 21/10/2021      KT
- Transfer to gitlab
#### 03/03/2022      KT
- Define defaults for transition metal guesses
- Add new variable core_updn
    + can be pair_core or invert_core
    + either pairs UP and DN core FODs or inverts them
    + applicable to atoms and molecules 
#### 06/04/2022      KT
- Completely reorganize the database entries
    + no more definitions for each element
    + summarized groups of elements, initialized those
    + Now also taking charge into account (as input, e.g. F_1- or Ni_2+)
    + TODO: Proper handling of transition metal atoms and charge
- Fixed bug for molecular systems in which an atom in not bound to any other atom -> lone FODs are now ok
#### 11/04/2022      KT
- Input allows now for charged systems, like F_1- or Ar_3+
- Additional options for input of species
    + ELEMENT_[charge]_[majority]_[configuration]_[core_electrons]    
    	* ELEMENT        ... (no default). element identifier, e.g., H, Ar, Se    
	* charge         ... (default = 0). positive or negative charge. Input is the number and the + or - sign, e.g., F_1-
	* majority       ... (default = UP). Indicate whether the UP or the DN channel is the majority, e.g., Ni_DN
	* configuration  ... (default depends on species). Can be 4s2, 4s1, or 3d4s. Used for transition metals to distinguish different electronic configurations, e.g. Cu_4s1 or Cu_4s2
	* core_electrons ... (default = None). Can be ECP_LC or ECP_SC, meaning large-core or small-core. Defines which FODs to remove from the core shells, e.g., Kr_ECP_LC
    + The order of these options is not relevant
    + You can use as many options at once as you want, e.g., Ge_3-_DN_ECP_LC
- Makes initialization more robust/easier
#### 10/03/2023      KT
- Minor bug fix for lone FODs in the DN channel -> now correctly initialized
